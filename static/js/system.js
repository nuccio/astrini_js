//
// System (Sun earth moon)
//

var System = function(root, scene) {
    // sun earth and moon plus sky and lights
    // and methods to do factual changes
    this.root = root;
    this.scene = scene;
    // variables to control the relative speeds of spinning and orbits in the
    // simulation
    this.ua =  UA_F;          // orbit scale (not realistic)
    this.moonax = MOONAX_F;

    this.earthTilt = EARTHTILT;
    //~ this.moonTilt = MOONTILT; // not used
    this.moonIncli = MOONINCL;
    this.moonIncliHard = MOONINCL_F;

    this.loadPlanets();
    this.system = [this.sun, this.earth, this.moon];
    this.orbitals = [this.earth, this.moon];

    this.loadSky();
    this.loadLight();

    this.tilted = false;
    this.inclined = false;
    this.inclinedHard = false;
}

System.prototype.loadPlanets = function() {
    this.sun = new Planetoid('sun', this.scene, this.root, SUNTEX,
    SUNRADIUS_F, SUNROT, 0);

    this.earth = new Orbital('earth', this.scene, this.root, EARTHTEX,
    EARTHRADIUS_F, 1, EARTHROTSET, this.root,
    EARTHREVO, EPHEMSIMPLESET, EARTHEPHEMDUMMYSET, this.ua);

    this.moon = new Orbital('moon', this.scene, this.root, MOONTEX,
    MOONRADIUS_F, MOONROT, MOONROTSET, this.earth.system,
    MOONREVO, MOONEPHEMSET, MOONEPHEMDUMMYSET, this.moonax);
}

System.prototype.loadLight = function() {

    // do not light sun
    this.sun.mod.material.disableLighting = true;
    // but sun is emitting
    this.sun.mod.material.emissiveColor = new BABYLON.Color3(0.9, 0.9, 0.9);
    // moon has no specularity
    this.moon.mod.material.specularColor = new BABYLON.Color3(0,0,0);
    // earth has special specular map
    this.earth.mod.material.specularTexture = new BABYLON.Texture(EARTHSPEC, this.scene);
    this.earth.mod.material.specularPower = SPECPOWER;
    // and emissive map
    this.earth.mod.material.emissiveTexture = new BABYLON.Texture(EARTHEMI, this.scene);

    // shadow casting light from the sun
    this.light = new BABYLON.DirectionalLight("sun_light",
        new BABYLON.Vector3(0, 0, 0), this.scene);
    this.light.shadowMinZ = UA / 2;
    this.light.shadowMaxZ = UA * 2;

    this.shadowGenerator = new BABYLON.ShadowGenerator(SHADOWMAP_RES, this.light);
    // remove self shadowing acnea
    this.shadowGenerator.bias = SHADOW_BIAS_F;

    // reference casting objects
    this.shadowGenerator.addShadowCaster(this.moon.mod);
    this.shadowGenerator.addShadowCaster(this.earth.mod);
    // reference receiving objects
    this.earth.mod.receiveShadows = true;
    this.moon.mod.receiveShadows = true;
}

System.prototype.placeLight = function() {
    this.light.setDirectionToTarget(this.earth.mod.absolutePosition);
}

System.prototype.loadSky = function() {
    this.sky = BABYLON.Mesh.CreateBox("sky_box", SKYSIZE, this.scene);

    // load skybox material and textures
    var skyMaterial = new BABYLON.StandardMaterial(this.name, this.scene);
    skyMaterial.backFaceCulling = false;
    skyMaterial.disableLighting = true;
    skyMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
    skyMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
    skyMaterial.reflectionTexture = new BABYLON.CubeTexture(SKYTEX, this.scene);
    skyMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    this.sky.material = skyMaterial;
    this.sky.infiniteDistance = true;

    // hidden by default
    this.has_sky = false;
    this.sky.visibility = 0;
}

System.prototype.showSky = function() {
    if (! this.has_sky) {
        show(this.sky, this.scene);
        this.has_sky = true;
    }
}

System.prototype.hideSky = function() {
    if (this.has_sky) {
        hide(this.sky, this.scene);
        this.has_sky = false;
    }
}

System.prototype.showShadows = function() {
    for (obj in this.orbitals) {
        this.orbitals[obj].showShadow();
    }
}

System.prototype.hideShadows = function() {
    for (obj in this.orbitals) {
        this.orbitals[obj].hideShadow();
    }
}

System.prototype.showMarkers = function() {
    for (obj in this.orbitals) {
        this.orbitals[obj].showOrbit();
        // let the stage displayTask do the showing
        // as it depends on collisions
        //~ this.orbitals[obj].showAxis();
    }
    //~ this.sun.showAxis();
}

System.prototype.hideMarkers = function() {
    for (obj in this.orbitals){
        this.orbitals[obj].hideAxis();
        this.orbitals[obj].hideOrbit();
    }
    this.sun.hideAxis();
}

System.prototype.place = function() {
    for (obj in this.system) {
        this.system[obj].place();
    }
    this.placeLight();
    this.sky.scaling = new BABYLON.Vector3(10*this.earth.distance,
        10*this.earth.distance,10*this.earth.distance);
}

System.prototype.rotate = function(time) {
    for (obj in this.system) {
        this.system[obj].rotate(time);
    }
}

System.prototype.lockTask = function() {
    // alignment contraints//
    // casted shadows should remain aligned with sun
    this.earth.shadow.position = this.earth.mod.absolutePosition;
    this.moon.shadow.position = this.moon.mod.absolutePosition;

    this.earth.shadow.rotation.y = this.earth.root.rotation.y;
    this.moon.shadow.rotation.y = this.earth.root.rotation.y;
}

System.prototype.add_tilt = function() {
    // add earth tilt
    generic_animate(this.scene, 'tilt_on', this.earth.dummy,
        'rotation.x', this.earth.dummy.rotation.x, EARTHTILT, FACTLEN, 0);
}

System.prototype.rem_tilt = function() {
    // remove earth tilt
    generic_animate(this.scene, 'tilt_off', this.earth.dummy,
        'rotation.x', this.earth.dummy.rotation.x, 0, FACTLEN, 0);
}

System.prototype.add_inclination = function(hard) {
    // add moon inclination (exagerated or not)
    if (!hard) {
        var value = MOONINCL;
    } else {
        var value = MOONINCL_F;
    }
    generic_animate(this.scene, 'incl_on', this.moon.dummy_root,
        'rotation.x', this.moon.dummy_root.rotation.x, value, FACTLEN, 0);
}

System.prototype.rem_inclination = function() {
    // remove moon inclination
    generic_animate(this.scene, 'incl_off', this.moon.dummy_root,
        'rotation.x', this.moon.dummy_root.rotation.x, 0, FACTLEN, 0);
}
