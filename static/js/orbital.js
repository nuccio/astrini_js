//
// Orbital class :
//planetoid with an orbit, shadow and
//distance from orbit root

// Define the orbital constructor inheriting from Planetoid
function Orbital(name, scene, root, tex, radius, period, offset,
    root_system, orbit_period, orbit_offset, orbit_dummy_offset,
    distance) {
    // Call the parent constructor, making sure (using Function#call)
    // that 'this' is set correctly during the call
    Planetoid.call(this, name, scene, root, tex, radius, period, offset);

    this.root_system = root_system;
    this.orbit_period = orbit_period;
    this.orbit_offset = orbit_offset;
    this.orbit_dummy_offset = orbit_dummy_offset;
    this.distance = distance;

    this.last_shadow_flip = 0;

    this.loadDummy();
    this.loadShadow();
    this.loadOrbit();
}

// Create a Orbital.prototype object that inherits from Planetoid.prototype.
Orbital.prototype = Object.create(Planetoid.prototype);

// Set the 'constructor' property to refer to Orbital
Orbital.prototype.constructor = Orbital;

Orbital.prototype.loadDummy = function() {
    //Create the dummy nodes, the skeleton of the system//
    this.dummy_root = new BABYLON.AbstractMesh('dummy_root', this.scene);
    // compass
    this.dummy_root.position = this.root_system.absolutePosition;
    this.dummy_root.rotation.y = this.orbit_dummy_offset;

    this.root = new BABYLON.AbstractMesh('root', this.scene);
    this.root.parent = this.dummy_root;

    this.system = new BABYLON.AbstractMesh('system', this.scene);
    this.system.parent = this.root;

    this.dummy = new BABYLON.AbstractMesh('dummy', this.scene);
    this.dummy.position = this.system.absolutePosition;

    //parent to get relative positionning
    this.mod.parent = this.dummy;
}

Orbital.prototype.orbit = function(julian_time) {
    if (!DEBUGNOORBIT) {
        var coord = ((-(Math.PI / this.orbit_period) * julian_time % Math.PI*2)
         + this.orbit_offset);
        this.root.rotation = new BABYLON.Vector3(0 , coord, 0);
    }
}

Orbital.prototype.rotate = function(julian_time) {
    //rotate on itself and around gravity center//
    //rotate on itself according to time//
    if (!DEBUGNOROTATE) {
        Planetoid.prototype.rotate.call(this, julian_time);
        this.orbit(julian_time);
    }
}

Orbital.prototype.place = function() {
    //position and scale
    Planetoid.prototype.place.call(this); // Parent method
    this.shadow.scaling = new BABYLON.Vector3(SHADOWLEN,this.radius,this.radius);
    this.system.position = new BABYLON.Vector3(this.distance,0,0);
    this.orbit_line.scaling = new BABYLON.Vector3(this.distance,this.distance,this.distance);
}

Orbital.prototype.loadShadow = function() {
    //black areas to show the casted sadows//
    this.shadow = makeTube(1, this.name, this.scene);
    // hidden by default
    this.has_shadow = false;
    this.shadow.visibility = 0;
}

Orbital.prototype.showShadow = function() {
    var now = new Date().getTime();
    if (! this.has_shadow && now > this.last_shadow_flip + SHADOWFADELEN * 2) {
        this.last_shadow_flip = now;
        show(this.shadow, this.scene);
        this.has_shadow = true;
    }
}

Orbital.prototype.hideShadow = function() {
    var now = new Date().getTime();
    if (this.has_shadow && now > this.last_shadow_flip + SHADOWFADELEN * 2) {
        this.last_shadow_flip = now;
        hide(this.shadow, this.scene);
        this.has_shadow = false
    }
}

Orbital.prototype.loadOrbit = function() {
    //Draw orbits
    this.orbit_line = makeArc(360, ORBITRESOLUTION, this.name, this.scene);
    //~ this.orbit_line.setHpr( 0, 90,0)
    this.orbit_line.parent = this.root;
    // hidden by default
    this.has_orbit = false;
    this.orbit_line.alpha = 0;
}

Orbital.prototype.showOrbit = function() {
    if (! this.has_orbit) {
        show_alpha(this.orbit_line, this.scene);
        this.has_orbit = true;
    }
}

Orbital.prototype.hideOrbit = function() {
    if (this.has_orbit) {
        hide_alpha(this.orbit_line, this.scene);
        this.has_orbit = false;
    }
}

