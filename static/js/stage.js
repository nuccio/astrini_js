//
// Stage
//

var Stage = function(scene, camera) {
    // system with camera, time and scale
    this.scene = scene;
    this.camera = camera;
    // init system
    this.initAbstract();
    this.sys = new System(this.root, this.scene);
    // init camera targets and orientation
    this.initCamera();
    // time Control
    this.local_time = new Date().getTime();
    this.timeTravel = false; // lock when changing speed
    this.cameraTravel = false; // lock when camera is auto-jumping
    this.cameraMoving = false; // lock when camera is between objects
    //~ this.sequences = []; // sequences to play when timeTraveling
    this.paused = false;
    this.reverse = false;
    this.jumping = false;
    this.previous_speed = 1.0;

    if (DEBUGNOTIME) {
        local_date = new Date(DEBUGSTARTTIME);
        // Set GMT time and not local time
        this.simul_time = local_date.getTime() - local_date.getTimezoneOffset()
            * 60000;
        this.simul_speed = 0;
        this.paused = true;
    } else {
        this.time_is_now();
    }

    // start with unrealistic scaling
    // 0 -> fantasist, 1 -> realist
    this.scale = 0.0;

    if (DEBUG_INTERFACE) {
        this.show_shadows = true;
        this.show_stars = false;
        this.show_markers = true;
    } else {
        this.show_shadows = false;
        this.show_stars = false;
        this.show_markers = false;
    }
    this.finished_loading = false;
    // keep list of textures
    this.scene.allTextures = [
        this.sys.sun.mod.material.diffuseTexture,
        this.sys.earth.mod.material.diffuseTexture,
        this.sys.moon.mod.material.diffuseTexture,
        this.sys.earth.mod.material.specularTexture,
        this.sys.earth.mod.material.emissiveTexture,
        this.sys.sky.material.reflectionTexture];
}

//
// Dummy nodes and camera drivers
//
Stage.prototype.initAbstract = function() {
    //Create the dummy nodes
    this.root = new BABYLON.AbstractMesh('main_root', this.scene);
    this.home = new BABYLON.AbstractMesh('home', this.scene);
    this.home.parent = this.root;
    this.focus = new BABYLON.AbstractMesh('focus', this.scene);
    this.focus.parent = this.root;
    this.hook = new BABYLON.AbstractMesh('hook', this.scene);
    this.hook.parent = this.root;
}


//
// Camera setup
//
Stage.prototype.placeCameraHome = function() {
    //Compute camera-sun distance from fov
    //~ var fov = this.camera.fov;
    var ua = this.sys.earth.distance;
    this.home.position = new BABYLON.Vector3(2.2 * ua, ua, 0);
}

Stage.prototype.initCamera = function() {
    this.camera.speed = CAMERA_SPEED;
    this.camera.maxZ = CAMERA_FAR;
    this.camera.minZ = CAMERA_NEAR;
    this.camera.fov = CAMERA_FOV_DEFAULT;
    // initial links for camera drivers
    if (!DEBUGCAM) {
        this.hook_target = this.home;
        this.focus_target = this.sys.sun;
        // align camera to center of scene
        this.placeCameraHome()
        this.camera.position = this.home.position.clone();
        this.camera.lockedTarget = this.sys.sun.mod;
        // begin in free cam mode
        this.free_camera = true;
    } else {
        // custom init
        this.hook_target = this.sys.moon;
        this.focus_target = this.sys.earth;
        this.placeCameraHome()
        this.camera.position = this.sys.sun.mod.position.clone();
        this.camera.lockedTarget = this.sys.earth.mod;
        this.free_camera = false;
    }
    // Set touch sensitivity
    this.camera.inputs.attached.touch.touchAngularSensibility = 90000;
}

//
// Camera animations
//

Stage.prototype.camerajump = function() {
    // change camera focus and position smoothly
    // callbacks
    function end_callback() {
        scene.stage.cameraTravel = false;
    }

    function faster_callback() {
        scene.stage.cameraMoving = false;
        // lock on new target after jump
        scene.stage.hook_target = scene.stage.new_hook_target;
        scene.stage.focus_target = scene.stage.new_focus_target;
        // restart flow of time
        var start = generic_animation('time_start', 'simul_speed',
            0, prev_speed, FREEZELEN);
        scene.beginDirectAnimation(scene.stage, [start], 0, FREEZELEN,
            false, 1, end_callback);
    }

    function jump_callback() {
        scene.stage.cameraMoving = true;
        var focus_jump = generic_vector_animation('focus_jump', 'position',
            scene.stage.focus.position, scene.stage.new_focus_target.absolutePosition,
            CAMTRAVELLEN);
        scene.beginDirectAnimation(scene.stage.focus, [focus_jump], 0, CAMTRAVELLEN);
        var hook_jump = generic_vector_animation('hook_jump', 'position',
            scene.stage.hook.position, scene.stage.new_hook_target.absolutePosition,
            CAMTRAVELLEN);
        scene.beginDirectAnimation(scene.stage.hook, [hook_jump], 0, CAMTRAVELLEN,
            false, 1, faster_callback);
    }
    // check lock to prevent simultaneous camera jumps
    if (this.cameraTravel === false) {
        this.cameraTravel = true;
        var prev_speed = this.simul_speed;
        // coming from free cam mode ?
        if (this.free_camera === true) {
            this.focus_target = null;
            this.hook_target = null;
            // create focus point in front of camera
            var cam_ray = this.camera.getForwardRay();
            this.hook.position = cam_ray.origin;
            this.focus.position = cam_ray.origin.add(
                cam_ray.direction.scale(this.sys.earth.distance));
        }

        // slow down, jump, restore speed (sequence with callbacks)
        var slower = generic_animation('time_stop', 'simul_speed',
            prev_speed, 0, FREEZELEN);
        scene.beginDirectAnimation(this, [slower], 0, FREEZELEN, false, 1,
            jump_callback);
    }
}

//
// Camera logic
//

Stage.prototype.look = function(object) {
    // we cannot go to and look at the same object
    // so we reverse roles
    if ((this.focus_target != object || this.free_camera) && !this.cameraTravel) {
        if (object === this.hook_target) {
            this.new_hook_target = this.focus_target;
        } else {
            this.new_hook_target = this.hook_target;
        }
        this.new_focus_target = object;
        this.camerajump();
        this.free_camera = false;
    }
}

Stage.prototype.go = function(object) {
    if ((this.hook_target != object || this.free_camera) && !this.cameraTravel) {
        if (object === this.focus_target) {
            // going from home to currently looking shouldn't turn cam toward home
            if (this.hook_target.name === 'home') {
                if (object.name === 'earth') {
                    this.new_focus_target = this.sys.sun;
                } else {
                    // default to earth
                    this.new_focus_target = this.sys.earth;
                }
            } else {
                this.new_focus_target = this.hook_target;
            }
        } else {
            this.new_focus_target = this.focus_target;
        }
        this.new_hook_target = object;
        this.camerajump();
        this.free_camera = false;
    }
}

Stage.prototype.changeFov = function(factor) {
    // Set camera FOV according to slider
    // 0 < factor < 100
    value = ((CAMERA_FOV_MIN - CAMERA_FOV_MAX) / 100) * factor + CAMERA_FOV_MAX;
    this.camera.fov = value;
}
//
// Timing control :
//

Stage.prototype.time_is_now = function() {
    this.simul_speed = 1;
    this.simul_time = new Date().getTime();
    this.paused = false;
    this.reverse = false;
}

Stage.prototype.changeSpeed = function(factor) {
    // if simulation is paused change previous speed
    if (! this.paused) {
        var speed = this.simul_speed * factor;
        if (speed > MAXSPEED) {
            this.simul_speed = MAXSPEED;
        } else if (speed < -MAXSPEED) {
            this.simul_speed = -MAXSPEED;
        } else {
            this.simul_speed = speed;
        }
    } else {
        speed = this.previousSpeed * factor;
        if (speed > MAXSPEED) {
            this.previousSpeed = MAXSPEED;
        } else if (speed < -MAXSPEED) {
            this.previousSpeed = -MAXSPEED;
        } else {
            this.previousSpeed = speed;
        }
    }
}

Stage.prototype.setSpeed = function(speed) {
    if (speed <= MAXSPEED) {
        this.simul_speed = speed;
    }
}

Stage.prototype.toggleSpeed = function() {
    if (! this.paused) {
        this.previousSpeed = this.simul_speed;
        this.simul_speed = 0.;
        this.paused = true;
    } else {
        this.simul_speed = this.previousSpeed;
        this.paused = false;
    }
}

Stage.prototype.reverseSpeed = function() {
    this.changeSpeed(-1);
    //button appearance should reflect reversed state
    if (! this.reverse) {
        this.reverse = true;
    } else {
        this.reverse = false;
    }
}

Stage.prototype.time_jump = function(jump_len) {
    // jump softly in time
    var delta = days_to_ms(jump_len);
    var prev_speed = this.simul_speed;

    // callbacks
    function end_callback() {
        scene.stage.timeTravel = false;
    }

    function start_callback() {
        var start = generic_animation('time_start', 'simul_speed',
            0, prev_speed, FREEZELEN);
        scene.beginDirectAnimation(scene.stage, [start], 0, FREEZELEN,
            false, 1, end_callback);
    }

    function jump_callback() {
        var jump = generic_animation('time_jump', 'simul_time',
            scene.stage.simul_time, scene.stage.simul_time + delta,
            TIMEJUMPLEN);
        scene.beginDirectAnimation(scene.stage, [jump], 0, TIMEJUMPLEN,
            false, 1, start_callback);
    }
    // datetime object has a maximum supported value
    // and check lock to prevent simultaneous timejumps
    if (this.simul_time + delta < TIMEMAX && this.timeTravel === false) {
        this.timeTravel = true;
        // slow down, jump, restore speed (sequence with callbacks)
        var stop = generic_animation('time_stop', 'simul_speed',
            prev_speed, 0, FREEZELEN);
        scene.beginDirectAnimation(this, [stop], 0, FREEZELEN, false, 1,
            jump_callback);
    }
}

Stage.prototype.timeTask = function() {
    // get passed time
    var new_time = new Date().getTime();
    var dt = new_time - this.local_time;
    this.local_time = new_time;
    // do not update sim time from speed when time jump in course
    if (! this.jumping) {
        // keep simulation time updated each frame
        this.simul_time +=  dt * this.simul_speed;
        // datetime object is limited between year 1 and year 9999
        if (this.simul_time < TIMEMIN) {
            this.simul_time =  TIMEMIN;
            this.simul_speed = 0.;
        } else if (this.simul_time > TIMEMAX) {
            this.simul_time = TIMEMAX;
            this.simul_speed = 0.;
        }
    }

}

//
// Scaling control
//

Stage.prototype.scale_on = function() {
    // animate scale of stage toward realistic
    generic_animate(this.scene, 'scale_on', this, 'scale',
        this.scale, 1, FACTLEN, 0);
}

Stage.prototype.scale_off = function() {
    // animate scale of stage toward realistic
    generic_animate(this.scene, 'scale_off', this, 'scale',
        this.scale, 0, FACTLEN, 0);
}

Stage.prototype.scaleSystemTask = function() {
    // scale the whole system from fantasist to realistic according
    // to this.scale (between 0. and 1.0)
    // todo : optimize by checking for scaling in progress to do the computations
    this.sys.earth.distance = linInt(this.scale, UA, UA_F);
    this.sys.earth.radius = linInt(this.scale, EARTHRADIUS, EARTHRADIUS_F);
    this.sys.moon.radius = linInt(this.scale, MOONRADIUS, MOONRADIUS_F);
    this.sys.sun.radius = linInt(this.scale, SUNRADIUS, SUNRADIUS_F);
    this.sys.moon.distance = linInt(this.scale, MOONAX, MOONAX_F);
    // adapt shadow bias to remove self shadowing acnea
    this.sys.shadowGenerator.bias = linInt(this.scale, SHADOW_BIAS, SHADOW_BIAS_F);
    // adapt camera settings
    this.camera.speed = linInt(this.scale, CAMERA_SPEED, CAMERA_SPEED_F);
    this.camera.maxZ = linInt(this.scale, CAMERA_FAR, CAMERA_FAR_F);
    this.camera.minZ = linInt(this.scale, CAMERA_NEAR, CAMERA_NEAR_F);
}

Stage.prototype.placeTask = function(task) {
    // position objects
    this.sys.place();
    this.sys.rotate(time_to_julian(this.simul_time));
    this.sys.lockTask();
    this.scaleSystemTask();
}

//
// Visualisation control
//

Stage.prototype.showShadows = function() {
    this.show_shadows = true;
}

Stage.prototype.hideShadows = function() {
    this.show_shadows = false;
}

Stage.prototype.showStars = function() {
    this.sys.showSky();
    this.show_stars = true;
}

Stage.prototype.hideStars = function() {
    this.sys.hideSky()
    this.show_stars = false;
}

Stage.prototype.showMarkers = function() {
    this.sys.showMarkers();
    this.show_markers = true;
}

Stage.prototype.hideMarkers = function() {
    this.sys.hideMarkers();
    this.show_markers = false;
}

Stage.prototype.lockCameraTask = function() {
    // keep home in place
    this.placeCameraHome()
    // update whole scene graph before camera positioning
    // (prevent jittering)
    this.sys.root.computeWorldMatrix(true);

    this.sys.earth.dummy_root.computeWorldMatrix(true);
    this.sys.earth.root.computeWorldMatrix(true);
    this.sys.earth.system.computeWorldMatrix(true);
    this.sys.earth.dummy.computeWorldMatrix(true);
    this.sys.earth.mod.computeWorldMatrix(true);
    this.sys.earth.axis.computeWorldMatrix(true);

    this.sys.moon.dummy_root.computeWorldMatrix(true);
    this.sys.moon.root.computeWorldMatrix(true);
    this.sys.moon.system.computeWorldMatrix(true);
    this.sys.moon.dummy.computeWorldMatrix(true);
    this.sys.moon.mod.computeWorldMatrix(true);
    this.sys.moon.axis.computeWorldMatrix(true);

    if (!this.free_camera) {
        // if camera drivers hooked to meshes and camera isn't auto-moving
        if (this.focus_target != null && !this.cameraMoving) {
            this.focus.position = this.focus_target.absolutePosition;
        }
        if (this.hook_target != null && !this.cameraMoving) {
            this.hook.position = this.hook_target.absolutePosition;
        }

        // keep camera aligned
        this.camera.position = this.hook.position.clone();
        this.camera.lockedTarget = this.focus;

    } else if (this.camera.lockedTarget != null) {
        // free camera gracefully while preserving orientation
        var v = this.camera.position.clone();
        v.subtractInPlace(this.camera.lockedTarget.position)
        this.camera.lockedTarget = null;

        this.camera.rotation.y = -Math.atan2(v.z, v.x) - Math.PI / 2;
        var len = Math.sqrt(v.x * v.x + v.z * v.z);
        this.camera.rotation.x = Math.atan2(v.y, len);
    }
}

Stage.prototype.displayTask = function() {
    // specific hide / show according to camera position
    // todo: optimize by not checking for collisions on every frame...

    // check for collision with camera
    var intersect = null;
    for (i in this.sys.system) {
        var obj = this.sys.system[i]
        if (obj.mod.intersectsPoint(this.camera.position)) {
            intersect = obj;
        }
    }

    var hide_shadows = false;
    if (intersect != null) {
        // remove intersecting mesh
        intersect.remove();
        // hide earth and moon shadows when looking from sun
        if (intersect.name === "sun") {
            hide_shadows = true;
            intersect.hideAxis();
        // hide shadow and axis marker if camera is intersecting
        } else {
            intersect.hideShadow();
            intersect.hideAxis();
        }
    }

    // show meshes and show/hide shadows & markers rules for unintersecting
    for (i in this.sys.system) {
        var obj = this.sys.system[i]
        // for all objects except intersect
        if (obj != intersect) {
            // show object
            obj.add();
            // shadows
            if (obj.name != "sun") {
                if (this.show_shadows && !hide_shadows) {
                        obj.showShadow();
                } else {
                        obj.hideShadow();
                }
            }
            // markers
            if (this.show_markers) {
                obj.showAxis();
            } else {
                obj.hideAxis();
            }
        }
    }
}

Stage.prototype.mainTask = function() {
    // update scene state at various levels in right order
    this.timeTask();
    this.placeTask();
    this.displayTask();
    this.lockCameraTask();
}
