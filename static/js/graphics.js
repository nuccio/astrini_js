function makeArc(angleDegrees, numSteps, name, scene) {
    // Create point list
    var points = [];
    var angleRadians = deg2rad(angleDegrees);
    for (var index = 0; index < numSteps + 1; index++) {
        var a = angleRadians * index / numSteps;
        points.push(new BABYLON.Vector3(Math.cos(a), 0, Math.sin(a)));
    }
    // create line mesh
    var arc = BABYLON.Mesh.CreateLines(name+"_arc", points, scene, true);
    arc.color = new BABYLON.Color3(MARKERCOLOR[0], MARKERCOLOR[1], MARKERCOLOR[2]);
    return arc;
}

function makeCircleFrame(numSteps, name, scene) {
    // Create point list
    var points = [];
    for (var index = 0; index < numSteps + 1; index++) {
        var a = Math.PI * 2 * index / numSteps;
        points.push(new BABYLON.Vector3(Math.cos(a), 0, Math.sin(a)));
    }
    for (var index = 0; index < numSteps + 1; index++) {
        var a = Math.PI * 2 * index / numSteps;
        points.push(new BABYLON.Vector3(Math.cos(a),Math.sin(a), 0));
    }
    // create line mesh
    var frame = BABYLON.Mesh.CreateLines(name+"_arcframe", points, scene, true);
    frame.color = new BABYLON.Color3(MARKERCOLOR[0], MARKERCOLOR[1], MARKERCOLOR[2]);
    return frame;
}

function makeLine(length, name, scene) {
    var points = [
        new BABYLON.Vector3(0, 0,  -length/2.),
        new BABYLON.Vector3(0, 0,  length/2.)];
    var line = BABYLON.Mesh.CreateLines(name+"_line", points, scene, true);
    line.color = new BABYLON.Color3(MARKERCOLOR[0], MARKERCOLOR[1], MARKERCOLOR[2]);
    return line;
}

function makeCross(length, name, scene) {
    var points = [
        new BABYLON.Vector3(0, 0, -length/2.),
        new BABYLON.Vector3(0, 0, length/2.),
        new BABYLON.Vector3(0, 0, 0),
        new BABYLON.Vector3(0, -length/2., 0),
        new BABYLON.Vector3(0, length/2., 0),
        new BABYLON.Vector3(0, 0, 0),
        new BABYLON.Vector3(-length/2., 0, 0),
        new BABYLON.Vector3(length/2., 0, 0)];
    var cross = BABYLON.Mesh.CreateLines(name+"_cross", points, scene, true);
    cross.color = new BABYLON.Color3(MARKERCOLOR[0], MARKERCOLOR[1], MARKERCOLOR[2]);
    return cross;
}

function makeTube(radius, name, scene) {
    var shadow = new BABYLON.Mesh.CreateTube(name+"_shadow",
        [new BABYLON.Vector3(0,0,0), new BABYLON.Vector3(1,0,0)],
        radius, SHADOWRES, null, 0, scene);
    //make dark transparent material
    var shadowMaterial = new BABYLON.StandardMaterial(name+"_shadow", scene);
    shadowMaterial.alpha = SHADOWALPHA;
    shadowMaterial.diffuseColor = new BABYLON.Color3(0,0,0);
    shadowMaterial.specularColor = new BABYLON.Color3(0,0,0);
    //~ shadowMaterial.specularPower = 0;
    shadowMaterial.backFaceCulling = false;
    shadow.material = shadowMaterial;
    return shadow;
}

