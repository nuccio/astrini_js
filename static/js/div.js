//
// Diverse functions
//

// babylon animations functions

function hide(object, scene) {
    // create disappearing animation
    var fadeOut = new BABYLON.Animation('fadeOut', 'visibility',
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

    fadeOut.setKeys([{frame: 0,value: 1},
        {frame: SHADOWFADELEN, value: 0}]);

    scene.beginDirectAnimation(object, [fadeOut], 0, SHADOWFADELEN);
}

function show(object, scene) {
    // and fade in
    var fadeIn = new BABYLON.Animation('fadeIn', 'visibility',
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

    fadeIn.setKeys([{frame: 0,value: 0},
        {frame: SHADOWFADELEN, value: 1}]);

    scene.beginDirectAnimation(object, [fadeIn], 0, SHADOWFADELEN);
}

// Alpha versions for axis and orbit lines
function hide_alpha(object, scene) {
    // create disappearing animation
    var fadeOut = new BABYLON.Animation('fadeOut', 'alpha',
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

    fadeOut.setKeys([{frame: 0, value: 1}, {frame: FADELEN, value: 0}]);

    scene.beginDirectAnimation(object, [fadeOut], 0, FADELEN);
}

function show_alpha(object, scene) {
    //and fade in
    var fadeIn = new BABYLON.Animation('fadeIn', 'alpha',
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

    fadeIn.setKeys([{frame: 0,value: 0}, {frame: FADELEN, value: 1}]);

    scene.beginDirectAnimation(object, [fadeIn], 0, FADELEN);
}

function generic_animate(scene, name, object, attribute,
    from_value, to_value, duration, offset) {
    // generate and launch animation on object's attribute
    var anim = new BABYLON.Animation(name, attribute,
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // keyframes
    anim.setKeys([{frame: 0,value: from_value},
        {frame: duration, value: to_value}]);
    // easing
    var easingFunction = new BABYLON.QuadraticEase();
    easingFunction.setEasingMode(
        BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    anim.setEasingFunction(easingFunction);
    // launch
    scene.beginDirectAnimation(
        object, [anim], offset, duration + offset);
}

function generic_animation(name, attribute, from_value, to_value,
    duration) {
    // return animation object :
    // smooth anim of some attribute
    var anim = new BABYLON.Animation(name, attribute,
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // keyframes
    anim.setKeys([{frame: 0,value: from_value},
        {frame: duration, value: to_value}]);
    // easing
    var easingFunction = new BABYLON.QuadraticEase();
    easingFunction.setEasingMode(
        BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    anim.setEasingFunction(easingFunction);

    return anim;
}

function generic_vector_animation(name, attribute, from_vector,
    to_vector, duration) {
    // return animation object on 3D vector :
    // smooth anim of some attribute
    var anim = new BABYLON.Animation(name, attribute,
        ANIMATIONS_FRAMERATE,
        BABYLON.Animation.ANIMATIONTYPE_VECTOR3,
        BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // keyframes
    anim.setKeys([{frame: 0,value: from_vector},
        {frame: duration, value: to_vector}]);
    // easing
    var easingFunction = new BABYLON.QuadraticEase();
    easingFunction.setEasingMode(
        BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    anim.setEasingFunction(easingFunction);

    return anim;
}


// BJS interaction effects

// Over/Out effect
var makeOverOut = function (mesh) {
    mesh.actionManager = new BABYLON.ActionManager(window.scene);

    var highlight = new BABYLON.InterpolateValueAction(
        BABYLON.ActionManager.OnPointerOverTrigger, mesh.material,
        "emissiveColor", BABYLON.Color3.White(), 100);
    var lowlight = new BABYLON.InterpolateValueAction(
        BABYLON.ActionManager.OnPointerOutTrigger, mesh.material,
        "emissiveColor", mesh.material.emissiveColor, 100);

    mesh.actionManager.registerAction(highlight);
    mesh.actionManager.registerAction(lowlight);
}

var load_over_effects = function () {
    makeOverOut(window.scene.stage.sys.earth.mod);
    makeOverOut(window.scene.stage.sys.moon.mod);
    makeOverOut(window.scene.stage.sys.sun.mod);
}


// Math and time functions
//
function deg2rad(a) {
    if (a > 360) {
        a = 360;
    } else if (a < 0) {
        a = 0;
    }
    return (a * Math.PI / 180);
}

function rad2deg(a) {
    if (a > Math.PI*2) {
        a = Math.PI*2;
    } else if (a < 0) {
        a = 0;
    }
    return (a * 180 / Math.PI);
}

function linInt(level, v1, v2) {
    // linearly interpolate between v1 and v2
    // according to 0<level<1
    return v1 * level + v2 * (1 - level);
}

function stamp_to_time(t) {
    // posix timestamp to datetime object
    var date = new Date();
    return date.setTime(t);
}

function time_to_stamp(t) {
    // time object to posix timestamp
    return t.getTime();
}

function time_to_julian(t) {
    // posix stamp in days plus julian offset
    return t/86400000 + 2440587.5;
}

function days_to_ms(t) {
    return t * 86400000;
}

// String manip
function pad_nb(nb) {
    return (nb+"").padStart(2, "0");
}
