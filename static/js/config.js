var APPNAME = 'Astrini';
// UI column width = 25 em (12px)
// as defined in style.css
COL_WIDTH = 25 * 12;
// Alignment of celestial bodies configurations
// correction to align equinox in simple model
var EPHEMSIMPLESET = 0.322988971608076 + Math.PI;
var EARTHEPHEMDUMMYSET = 0;
var MOONEPHEMSET = 1.9; // correction to align moon phases
var MOONEPHEMDUMMYSET = 2.14; // correction to align moon north/south oscillation
var MOONROTSET = 4.05; // show correct side of moon
//var EARTHROTSET = 2.12; // Align with A
var EARTHROTSET = 3.17; // Align with B

// Debug flags to force time or positionning
var DEBUGCAM = false; // Special hook/focus targets, free cam
var DEBUGDISPLAY = false; // Show markers and shadows
var DEBUGNOROTATE = false; // Do not rotate planets
var DEBUGNOORBIT = false; // Do not orbit planets

var DEBUGNOTIME = false; // Do not align with real time
//var DEBUGNOTIME = true; // Do not align with real time
//var DEBUGSTARTTIME = '2019-03-20T21:58:25'; // march equinox
//var DEBUGSTARTTIME = '2019-06-21T15:54:14'; // june solstice
//var DEBUGSTARTTIME = '2019-06-03T12:59:00'; // greenwhich zenith A
//var DEBUGSTARTTIME = '2020-03-20T12:07:00'; // greenwhich zenith B
//var DEBUGSTARTTIME = '2020-12-14T16:10:00'; // complete solar eclipse on chili
//var DEBUGSTARTTIME = '2020-07-05T06:40:00'; // full moon

// Interface / performance settings
var DEBUG_BABYLON = false;
var DEBUG_INTERFACE = false;   //show menu by default

var AUTO_OPTIMIZE = false; // Breaks shadow caster !!
var ANTIALIASING = true;
var UI_REFRESH_DELAY = 150;
var OVER_EFFECT = false; // cpu intensive cursor detection over meshes
var SHADOWMAP_RES = 512; // resolution of shadow caster
var SHADOW_BIAS = 0.001; // self shadowing acnea removal with bias factor
var SHADOW_BIAS_F = 0.01; // adapt it for fantasist scaling

// Animations
var ANIMATIONS_FRAMERATE = 60 // framerate of babylonJS animation objects
// duration in frames of soft transitions
var FADELEN = 10; // markers
var SHADOWFADELEN = 20; // shadow tubes
var FREEZELEN = 20; // freeze warps
var CAMTRAVELLEN = 100; // camera travel
var TIMEJUMPLEN = 250; // timejumps
var FACTLEN = 20; // factual changes

// Marker and orbitlines settings
var ORBITRESOLUTION = 256;
var CIRCLEFRAMERES = 64;
var SHADOWLEN = 20000;

// Skinning
var SHADOWALPHA = 0.3;
var MARKERCOLOR = [0.7, 1, 0];
var SUNTEX = 'static/img/sunmap.jpg';
var EARTHTEX = 'static/img/earth/earthmap.jpg';
var EARTHSPEC = 'static/img/earth/earthspec.jpg';
var EARTHEMI = 'static/img/earth/earthlights.jpg';
var MOONTEX = 'static/img/moonmap.jpg';
var SKYTEX = 'static/img/skybox/star_field';
var SPECPOWER = 50;

//Primitives resolution
var SPHERERES = 24;
var SHADOWRES = 64;
var SKYRES = 512;
var SKYSIZE = 10;

//Time limits
var MAXSPEED = 100000000.0;
var TIMEMAX = 86400000 * 365 * 9999;
var TIMEMIN = 0;

// Camera near/far setting (depends on following multiplier)
// values for realist and fantasit scales
var CAMERA_FAR = 100000;
var CAMERA_FAR_F = 1000000;
var CAMERA_NEAR = 0.001;
var CAMERA_NEAR_F = 1;
var CAMERA_SPEED = 0.02;
var CAMERA_SPEED_F = 3;
// Camera Field of view
var CAMERA_FOV_MIN = Math.PI / 24;
var CAMERA_FOV_MAX = Math.PI / 4;
var CAMERA_FOV_DEFAULT = ((CAMERA_FOV_MIN - CAMERA_FOV_MAX) / 100) * 20 + CAMERA_FOV_MAX;
// multiplier to avoid working with very high numbers
var multiplier = 1 / 1000000.;

// realistic values
// cf Wikipedia
// space
var UA = 149597887 * multiplier;
var EARTHRADIUS = 6371 * multiplier;
var SUNRADIUS = 696342 * multiplier;
var MOONRADIUS = 1737 * multiplier;
var MOONAX = 384399 * multiplier;

var EARTHTILT = 23.44 * Math.PI/180;
//~ var MOONTILT = 6.68 * Math.PI/180;  // not used
var MOONINCL = 5.145 * Math.PI/180;
// time
var SECONDSINDAY = 86400.;
var SUNROT = 27.28; // in days
var MOONROT = 27.321582;
var MOONREVO = MOONROT;
var EARTHREVO = 365.25696;

// fantasist values
var UA_F = UA;
var EARTHRADIUS_F = 5.;
var SUNRADIUS_F = 7.;
var MOONRADIUS_F = 2.;
var MOONAX_F = (MOONRADIUS_F / SUNRADIUS_F) * UA_F ;// computed for full eclipses
var MOONINCL_F = MOONINCL * 3; // an exagerated inclination to avoid moon eclipses in fantasist scale
