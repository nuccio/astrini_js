//
// Main file for astrini application
//

function populate_scene(scene, canvas) {
    // create a universal (keyboard mouse gamepad) camera
    var camera = new BABYLON.UniversalCamera('camera', BABYLON.Vector3.Zero(), scene);
    // attach the camera to the canvas
    camera.attachControl(canvas, false);
    // keep reference of camera in global variable
    window.camera = camera;
    // create main object with stage control
    var stage = new Stage(scene, camera);
    // keep reference of stage in global variable
    window.stage = stage;

    if (DEBUG_BABYLON) {
        // show debug web interface
        scene.debugLayer.show();
    }
    return stage;
}

function main() {
    // get the canvas DOM element
    var canvas = document.getElementById('render-canvas');

    // load the 3D engine
    var engine = new BABYLON.Engine(canvas, ANTIALIASING);
    // keep reference of engine in global variable
    window.engine = engine;

    // custom loading screen
    var loadingScreen = new CustomLoadingScreen("Loading...");
    // replace the default loading screen
    engine.loadingScreen = loadingScreen;
    // show the loading screen
    engine.displayLoadingUI();

    // createScene function that creates and return the scene
    var createScene = function(){
        // create a basic BJS Scene object
        var scene = new BABYLON.Scene(engine);
        scene.clearColor = new BABYLON.Color3(0.5, 0.5, 0.5);
        // Enable glowing for emmissive objects
        scene.glow = new BABYLON.GlowLayer("glow", scene, {
                    mainTextureFixedSize: 256,
                    blurKernelSize: 64
                });
        scene.glow.intensity = 0.5;
        // add reference to scene system and planetoids etc...
        scene.stage = populate_scene(scene, canvas);

        // keep reference of scene in global variable
        window.scene = scene;

        if (OVER_EFFECT) {
            // now that scene is loaded
            load_over_effects();
        }

        // return the created scene
        return scene;
    }

    var scene = createScene();

    // run the render loop
    engine.runRenderLoop(function(){
        // update the scene
        scene.stage.mainTask();
        // update the ui
        ui_updateTask();
        // render
        scene.render();
    });
    // Layout init
    ui_make_layout();
    // the canvas/window resize event handler
    window.addEventListener('resize', function(){
        ui_make_layout();
    });

    if (AUTO_OPTIMIZE) {
        // auto optimize scene
        BABYLON.SceneOptimizer.OptimizeAsync(scene);
    }

}

// start babylonJS app when DOM is loaded
window.addEventListener('DOMContentLoaded', main());
