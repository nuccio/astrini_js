// Responsive panels on grid layout
// Initial layout
// Active panel can be none, tool, help, love
// tool+help or tool+love.
if (DEBUG_INTERFACE) {
    var ui_active_panel = "tool";
} else {
    var ui_active_panel = "none";
}

var see_tool = false;
var see_help = false;
var see_love = false;
var wide_limit = 16 / 9;
var portrait_limit = 1;

var wrapper = document.getElementById("main-wrapper");
var tool_panel = document.getElementById("tool-panel");
var help_panel = document.getElementById("help-panel");
var love_panel = document.getElementById("love-panel");
var love_btn = document.getElementById("love-btn");
var help_btn = document.getElementById("help-btn");
var render_canvas = document.getElementById("render-canvas");


function clear_layout() {
    // Remove panels and special layout classes
    wrapper.classList.remove("wrap-full");
    wrapper.classList.remove("wrap-3col");
    wrapper.classList.remove("wrap-2col-left");
    wrapper.classList.remove("wrap-2col-right");
    wrapper.classList.remove("wrap-2row");

    love_btn.classList.remove("btn-love-right");
    love_btn.classList.remove("btn-love-left");
    help_btn.classList.remove("btn-help-right");
    help_btn.classList.remove("btn-help-left");

    help_panel.classList.remove("order-1");
    love_panel.classList.remove("order-1");
    render_canvas.classList.remove("order-1");
    render_canvas.classList.remove("order-2");
    tool_panel.classList.remove("half-height");
    help_panel.classList.remove("half-height");
    love_panel.classList.remove("half-height");
    render_canvas.classList.remove("half-height");

    tool_panel.style.display = "none";
    help_panel.style.display = "none";
    love_panel.style.display = "none";
}

function get_screen_size() {
    var width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;
    var height = window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight;
    return [width, height]
}

function screen_category() {
    // Main canvas should never take less than half
    // the screen space
    var screen_w = get_screen_size()[0];
    var screen_h = get_screen_size()[1];
    var screen_a = screen_w * screen_h;
    var col_w = 25 * 12;
    var col_a = col_w * screen_h;
    var ratio = screen_w / screen_h;

    var screen = "wide";
    if (ratio < wide_limit &&
        ratio > portrait_limit) {
        if (screen_a - col_a < screen_a / 2) {
            screen = "portrait";
        } else {
            screen = "landscape";
        }
    } else if  (ratio < portrait_limit) {
        screen = "portrait";
    } else if (screen_a - 2 * col_a < screen_a/ 2) {
        screen = "landscape";
    }
    return screen;
}

function get_scaling() {
    var dpr = window.devicePixelRatio || 1;
    return 1 / dpr;
}

function ui_make_layout() {
    clear_layout();
    // Fix blurry canvas on hidpi screen
    window.engine.setHardwareScalingLevel(get_scaling());
    // Responsive
    var screen_cat = screen_category();
    if (screen_cat === "landscape") {
        love_btn.classList.add("btn-love-left");
        help_btn.classList.add("btn-help-left");
    } else {
        love_btn.classList.add("btn-love-right");
        help_btn.classList.add("btn-help-right");
    }

    if (screen_cat === "landscape" || screen_cat === "portrait") {
        if (ui_active_panel === "tool+help") {
            ui_active_panel = "tool";
            see_help = false;
        } else if (ui_active_panel === "tool+love") {
            ui_active_panel = "tool";
            see_love = false;
        }
    }
    if (ui_active_panel === "none") {
        wrapper.classList.add("wrap-full");
    } else if  (ui_active_panel === "tool") {
        if (screen_cat === "wide" || screen_cat === "landscape") {
            wrapper.classList.add("wrap-2col-left");
        } else {
            wrapper.classList.add("wrap-2row");
            render_canvas.classList.add("half-height");
            tool_panel.classList.add("half-height");
        }
        tool_panel.style.display = "block";
    } else if (ui_active_panel === "help") {
        if (screen_cat === "wide") {
            wrapper.classList.add("wrap-2col-right");
        } else if (screen_cat === "landscape") {
            wrapper.classList.add("wrap-2col-left");
            help_panel.classList.add("order-1");
            render_canvas.classList.add("order-2");
        } else {
            wrapper.classList.add("wrap-2row");
            render_canvas.classList.add("half-height");
            render_canvas.classList.add("order-2");
            help_panel.classList.add("half-height");
            help_panel.classList.add("order-1");
        }
        help_panel.style.display = "block";
    } else if (ui_active_panel === "love") {
        if (screen_cat === "wide") {
            wrapper.classList.add("wrap-2col-right");
        } else if (screen_cat === "landscape") {
            wrapper.classList.add("wrap-2col-left");
            love_panel.classList.add("order-1");
            render_canvas.classList.add("order-2");
        } else {
            wrapper.classList.add("wrap-2row");
            render_canvas.classList.add("half-height");
            render_canvas.classList.add("order-2");
            love_panel.classList.add("half-height");
            love_panel.classList.add("order-1");
        }
        love_panel.style.display = "block";
    } else if (ui_active_panel === "tool+help") {
        wrapper.classList.add("wrap-3col");
        tool_panel.style.display = "block";
        help_panel.style.display = "block";
    } else if (ui_active_panel === "tool+love") {
        wrapper.classList.add("wrap-3col");
        tool_panel.style.display = "block";
        love_panel.style.display = "block";
    }
    if (typeof window.engine !== 'undefined') {
        // Setup an engine resize in ui_updateTask
        resize_trigger = true;
    }
}

function ui_toggle_panel(name) {
    panel = document.getElementById(name);
    // Responsive
    var screen_cat = screen_category();
    if (name === "tool-panel") {
        if (see_tool) {
            see_tool = false;
            if (screen_cat === "wide") {
                if (see_help) {
                    ui_active_panel = "help";
                } else if (see_love) {
                    ui_active_panel = "love";
                } else {
                    ui_active_panel = "none";
                }
            } else {
                ui_active_panel = "none";
            }
        } else {
            see_tool = true;
            if (screen_cat === "wide") {
                if (see_help) {
                    ui_active_panel = "tool+help";
                } else if (see_love) {
                    ui_active_panel = "tool+love";
                } else {
                    ui_active_panel = "tool";
                }
            } else {
                ui_active_panel = "tool";
                see_help = false;
                see_love = false;
            }
        }
    }
    if (name === "help-panel") {
        if (see_help) {
            see_help = false;
            see_love = false;
            if (screen_cat === "wide") {
                if (see_tool) {
                    ui_active_panel = "tool";
                } else {
                    ui_active_panel = "none";
                }
            } else {
                see_tool = false;
                ui_active_panel = "none";
            }
        } else {
            see_help = true;
            see_love = false;
            if (screen_cat === "wide") {
                if (see_tool) {
                    ui_active_panel = "tool+help";
                } else {
                    ui_active_panel = "help";
                }
            } else {
                see_tool = false;
                ui_active_panel = "help";
            }
        }
    }
    if (name === "love-panel") {
        if (see_love) {
            see_love = false;
            see_help = false;
            if (screen_cat === "wide") {
                if (see_tool) {
                    ui_active_panel = "tool";
                } else {
                    ui_active_panel = "none";
                }
            } else {
                see_tool = false;
                ui_active_panel = "none";
            }
        } else {
            see_love = true;
            see_help = false;
            if (screen_cat === "wide") {
                if (see_tool) {
                    ui_active_panel = "tool+love";
                } else {
                    ui_active_panel = "love";
                }
            } else {
                see_tool = false;
                ui_active_panel = "love";
            }
        }
    }
    ui_make_layout();
}

// Loading screen
function CustomLoadingScreen() {
}
CustomLoadingScreen.prototype.displayLoadingUI = function() {
    document.getElementById("loading_screen").style.display = "block";
};
CustomLoadingScreen.prototype.hideLoadingUI = function() {
    document.getElementById("loading_screen").style.display = "none";
};

// Zoom slider
var zoom_slider = document.getElementById("zoom-range");
zoom_slider.oninput = function() {
    window.scene.stage.changeFov(parseFloat(this.value));
}

// Speed slider
var speed_slider = document.getElementById("speed-range");
speed_slider.oninput = function() {
    var value = parseFloat(this.value)
    speed_list = [0,
        1,
        100,
        1000,
        10000,
        100000,
        1000000,
        5000000,
        10000000,
        50000000,
        100000000];
    // Toggle Pause
    if (window.scene.stage.paused) {
        window.scene.stage.toggleSpeed()
    }
    // Consider reverse mode
    if (window.scene.stage.reverse) {
        window.scene.stage.setSpeed(-1 * speed_list[value]);
    } else {
        window.scene.stage.setSpeed(speed_list[value]);
    }
    update_speed_dispay();
}

// Pause / reverse / reset
function ui_reverse_speed() {
    window.scene.stage.reverseSpeed();
    update_speed_dispay();
}

function ui_toggle_speed() {
    window.scene.stage.toggleSpeed()
    update_speed_dispay();
}

function ui_reset_speed() {
    window.scene.stage.time_is_now()
    speed_slider.value = 1;
    update_speed_dispay();
}

// Camera locking
function ui_lock_camera() {
    // change stage state
    window.scene.stage.free_camera = false;
}

function ui_tog_free_camera() {
    // change stage state
    window.scene.stage.free_camera = true;
}

// look at locks
function ui_cam_lock_on(object) {
    window.scene.stage.look(object);
}

function ui_cam_go_to(object) {
    window.scene.stage.go(object);
}


// factual changes
var fact_earth = document.getElementById("fact_earth");
var fact_moon = document.getElementById("fact_moon");
var fact_moon2 = document.getElementById("fact_moon2");
var fact_scale = document.getElementById("fact_scale");

function ui_toggle_fact_earth() {
    // change system state
    if(fact_earth.checked){
        window.scene.stage.sys.add_tilt();
    }
    else {
        window.scene.stage.sys.rem_tilt();
    }
}

function ui_toggle_fact_scale() {
    // change stage scale
    if(fact_scale.checked){
        window.scene.stage.scale_on();
    }
    else {
        window.scene.stage.scale_off();
    }
}

function ui_toggle_fact_moon() {
    // both fact inclination changes can't happen at same time
    // so disable the other one
    if (fact_moon2 != null) {
        if(fact_moon2.checked){
            fact_moon2.checked = false;
        }
    }
    // change system state
    if(fact_moon.checked){
        window.scene.stage.sys.add_inclination(0);
    }
    else {
        window.scene.stage.sys.rem_inclination();
    }
}
function ui_toggle_fact_moon2() {
    // both fact inclination changes can't happen at same time
    // so disable the other one
    if (fact_moon != null) {
        if(fact_moon.checked){
            fact_moon.checked = false;
        }
    }
    // change system state
    if(fact_moon2.checked){
       window.scene.stage.sys.add_inclination(1);
    }
    else {
        window.scene.stage.sys.rem_inclination();
    }
}

// display options
var togshadows = document.getElementById("tog_shadows");
var togmarkers = document.getElementById("tog_markers");
var togstars = document.getElementById("tog_stars");
var togquality = document.getElementById("tog_quality");

function ui_toggle_shadows() {
    if(togshadows.checked){
       window.scene.stage.showShadows();
    } else {
        window.scene.stage.hideShadows();
    }
}

function ui_toggle_markers() {
    if(togmarkers.checked){
       window.scene.stage.showMarkers();
    } else {
        window.scene.stage.hideMarkers();
         }
}

function ui_toggle_stars() {
    if(togstars.checked){
       window.scene.stage.showStars();
         }
    else {
        window.scene.stage.hideStars();
         }
}

function ui_toggle_quality() {
    if(togquality.checked){
        window.engine.setHardwareScalingLevel(get_scaling() * 2);
    } else {
        window.engine.setHardwareScalingLevel(get_scaling());
    }
}


// camera logic
var look_earth = document.getElementById("look_earth").parentElement;
var look_moon = document.getElementById("look_moon").parentElement;
var look_sun = document.getElementById("look_sun").parentElement;
var go_earth = document.getElementById("go_earth").parentElement;
var go_moon = document.getElementById("go_moon").parentElement;
var go_sun = document.getElementById("go_sun").parentElement;
var go_ext = document.getElementById("go_ext").parentElement;


locks = [look_earth, look_moon, look_sun, go_earth, go_moon, go_sun, go_ext];

function clear_cam_locks() {
    for (i in locks) {
        locks[i].classList.remove("active");
    }
}

function activate_lock(lock) {
    lock.classList.add("active");
}

function update_speed_dispay() {
    var s = Math.trunc(window.scene.stage.simul_speed);
    var speed_display = document.getElementById("speed-display");
    speed_display.innerHTML = "x " + s;
}

function update_buttons() {
        // set buttons states and appearances according to user input
        // buttons should reflect what you're looking at and what you're following

        // deactivate cam locks buttons
        clear_cam_locks();

        // free cam button
        if (window.scene.stage.free_camera) {
            // add disabled button style
            var btn = document.getElementById("tog_free_camera")
            btn.classList.add("disabledbtn");
        } else {
            // remove disabled style
            var btn = document.getElementById("tog_free_camera");
            // remove btn disabled class
            btn.classList.remove("disabledbtn");

            // camlocks button activation
            if (window.scene.stage.hook_target === window.scene.stage.sys.earth) {
                // show activated button for followed object
                activate_lock(go_earth);
            } else if (window.scene.stage.hook_target === window.scene.stage.sys.moon) {
                activate_lock(go_moon);
            } else if (window.scene.stage.hook_target === window.scene.stage.sys.sun) {
                activate_lock(go_sun);
            } else if (window.scene.stage.hook_target === window.scene.stage.home) {
                activate_lock(go_ext);
            }


            if (window.scene.stage.focus_target === window.scene.stage.sys.earth) {
                // show activated button for looked object
                activate_lock(look_earth);
            } else if (window.scene.stage.focus_target === window.scene.stage.sys.moon) {
                activate_lock(look_moon);
            } else if (window.scene.stage.focus_target === window.scene.stage.sys.sun) {
                activate_lock(look_sun);
            }
        }
}

function update_dev_interface() {
    var dev_panel = document.getElementById("dev-panel");
    dev_panel.innerHTML = "<h3>Debug</h3> fps: "+Math.round(window.engine.getFps());
}

// UI Update procedure
//
var last_ui_flip = 0;
var resize_trigger = false;
var resize_pass = 1;

function ui_updateTask() {
    // update at given refresh rate
    var now = new Date().getTime()
    if (now > last_ui_flip + UI_REFRESH_DELAY) {
        last_ui_flip = now;
        // check if textures are loaded
        if (!window.scene.stage.finished_loading) {
            var loaded = true;
            for (i in window.scene.allTextures) {
                if (!window.scene.allTextures[i].isReady()) {
                    loaded = false;
                    break;
                }
            }
            if (loaded) {
                // hide loading screen
                window.engine.hideLoadingUI();
                window.scene.stage.finished_loading = true;
            }
        }

        // update simul time and speed display
        var t = new Date(window.scene.stage.simul_time);
        var time_display = document.getElementById("time-display");
        time_display.innerHTML = `<span class="time-label">Local:</span>
<span class="time-date">${t.getFullYear()}-\
${pad_nb(t.getMonth()+1)}-${pad_nb(t.getDate())}&ensp;${pad_nb(t.getHours())}:\
${pad_nb(t.getMinutes())}:${pad_nb(t.getSeconds())}</span><br>
<span class="time-label">Greenwich:</span>
<span class="time-date">${pad_nb(t.getUTCFullYear())}-${pad_nb(t.getUTCMonth()+1)}-\
${pad_nb(t.getUTCDate())}&ensp;${pad_nb(t.getUTCHours())}:\
${pad_nb(t.getUTCMinutes())}:${pad_nb(t.getUTCSeconds())}</span>`;
        // update buttons states
        update_buttons();
        // speed display
        update_speed_dispay();
        if (resize_trigger) {
            // Force refresh hardware scaling to fix buggy resize on firefox
            // Ugly non deterministic and multi pass :(
            window.engine.setHardwareScalingLevel(1.1);
            window.engine.resize();
            ui_toggle_quality();
            resize_pass += 1;
            if (resize_pass > 9) {
                resize_trigger = false;
                resize_pass = 1;
            }
        }
        // debug lines
        if (DEBUG_INTERFACE) {
            update_dev_interface();
        }
    }
}

//
// init user interface
//
// unchecking (cleaning)
var checkboxes = [togshadows, togstars, togmarkers, togquality, fact_earth, fact_moon, fact_moon2, fact_scale];
for (i in checkboxes) {
    if (checkboxes[i] != null) {
        checkboxes[i].checked = false;
    }
}
// slider reset (cleaning)
zoom_slider.value = 20;
speed_slider.value = 1;
