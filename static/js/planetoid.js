//
// Planetoid main class
//
var Planetoid = function(name, scene, root, tex, radius, period, offset) {
    //rotating spherical model with markers//
    this.name = name;
    this.scene  = scene;
    this.root = root;
    this.radius = radius;
    this.period = period;
    this.offset = offset;
    this.load(tex);
}

Planetoid.prototype.load = function(tex) {
    // create a built-in 'sphere' shape;
    // its constructor takes 5 params: name, subdivisions, diameter, scene
    this.mod = BABYLON.Mesh.CreateSphere(this.name, SPHERERES, 1, this.scene);
    // the Planetoid position is the position of this 3D model
    this.position = this.mod.position;
    this.absolutePosition = this.mod.absolutePosition;

    //load texture
    var sphereMaterial = new BABYLON.StandardMaterial(this.name, this.scene);
    sphereMaterial.diffuseTexture = new BABYLON.Texture(tex, this.scene);
    this.mod.material = sphereMaterial;

    //marker of orientation
    this.axis = makeCircleFrame(CIRCLEFRAMERES, this.name, this.scene);
    this.axis.parent = this.mod;
    // relative size to parent
    this.axis.scaling = new BABYLON.Vector3(0.51, 0.51, 0.51);
    // render in front
    //this.axis.renderingGroupId = 1;

    // not visible by default
    this.has_axis = false;
    this.axis.alpha = 0;
}

Planetoid.prototype.remove = function() {
    if (! this.removed) {
        this.scene.removeMesh(this.mod);
        this.removed = true;
    }
}

Planetoid.prototype.add = function() {
    if (this.removed) {
        this.scene.addMesh(this.mod);
        this.removed = false;
    }
}

Planetoid.prototype.showAxis = function() {
    if (! this.has_axis) {
        show_alpha(this.axis, this.scene);
        this.has_axis = true;
    }
}

Planetoid.prototype.hideAxis = function() {
    if (this.has_axis) {
        hide_alpha(this.axis, this.scene);
        this.has_axis = false;
    }
}

Planetoid.prototype.rotate = function(julian_time) {
    //rotate on itself according to time//
    if (!DEBUGNOROTATE) {
        this.mod.rotation = new BABYLON.Vector3(
            0, -(Math.PI / this.period) * julian_time % Math.PI*2 + this.offset, 0);
    }
}

Planetoid.prototype.place = function() {
    //set scale//
    this.mod.scaling = new BABYLON.Vector3(this.radius*2,this.radius*2,this.radius*2);
}
