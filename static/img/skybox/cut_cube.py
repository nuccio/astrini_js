# Cut cube projection to individual faces

import sys
from PIL import Image

imgIn = Image.open(sys.argv[1])

in_w, in_h = imgIn.size
w, h = (int(in_w/4), int(in_h/3))
print(w,h)
# imgOut.crop((2048, 0, 3072, 1024)).save(sys.argv[1].split('.')[0]+"_pz.jpg")
imgIn.crop((2*w, 0, 3*w, h)).save(sys.argv[1].split('.')[0]+"_py.jpg")
imgIn.crop((2*w, 2*h, 3*w, 3*h)).save(sys.argv[1].split('.')[0]+"_ny.jpg")
imgIn.crop((0, h, w, 2*h)).save(sys.argv[1].split('.')[0]+"_nz.jpg")
imgIn.crop((w, h, 2*w, 2*h)).save(sys.argv[1].split('.')[0]+"_nx.jpg")
imgIn.crop((2*w, h, 3*w, 2*h)).save(sys.argv[1].split('.')[0]+"_pz.jpg")
imgIn.crop((3*w, h, 4*w, 2*h)).save(sys.argv[1].split('.')[0]+"_px.jpg")
