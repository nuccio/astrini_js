# Astrini

Official website: https://lambda.casa/astrini_js

Astrini is a mini-educational software designed to assist teaching of basic notions in astronomy by explaining the mechanics of:

 - seasons
 - phases of the Moon and Earth
 - eclipses of Moon and Earth

## Dependencies:

	- Flask
	- Flask Babel
	- Flask Compress
	- Flask Caching
	- Flask Assets
	- jsmin
	- cssutils

Included:

	- BabylonJS
	- bootstrap
	- bootstrap-native
