#!/usr/bin/python3
# Std lib
import os
# Modules
from flask import Flask, render_template, request, redirect, url_for, jsonify
# i18n
from flask_babel import Babel
# Compression
from flask_compress import Compress
# Cache
from flask_caching import Cache
# Assets preprocessing and packing
from flask_assets import Environment, Bundle

# Separate secrets
import secret

app = Flask(__name__)
app.secret_key = secret.APP_SECRET_KEY

app_folder = os.path.split(os.path.abspath(__file__))[0]

babel = Babel(app)
LANGUAGES = {
    'en': 'English',
    'fr': 'French',
    'it': 'Italian'
}

# Enable gzip compression
Compress(app)
# Simple caching
cache = Cache(app,config={'CACHE_TYPE': 'simple'})
# Assets bundling
assets = Environment(app)

js = Bundle('js/lib/bootstrap-native.min.js',
	'js/config.js',
	'js/div.js',
	'js/graphics.js',
	'js/planetoid.js',
	'js/orbital.js',
	'js/system.js',
	'js/stage.js',
	'js/ui.js',
	'js/astrini.js',
	filters='jsmin',
        output='gen/packed_tail.js')

css = Bundle('css/bootstrap.min.css',
        'css/style.css',
	filters='cssutils',
        output='gen/packed_head.css')

assets.register('js_tail', js)
assets.register('css_head', css)

# send translation according to browser header
@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())

@app.route('/')
def main_page():
    return render_template('main.html')

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5005)
